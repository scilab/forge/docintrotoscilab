// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Measuring the time
tic(); lambda = spec(rand(200,200)); t = toc()
tic(); lambda = spec(rand(200,200)); t = toc()
tic(); lambda = spec(rand(200,200)); t = toc()
// A loop to average
for i = 1 : 10
  tic(); 
  lambda = spec(rand(200,200));
  t(i) = toc();
end
min(t)
max(t)
mean(t)
// Using timer
for i = 1 : 10
  timer(); 
  lambda = spec(rand(200,200));
  t(i) = timer();
end
min(t)
max(t)
mean(t)

