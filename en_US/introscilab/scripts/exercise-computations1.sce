// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Addition, multiplication and braces 
// Precedence of operators is the same in Scilab and in mathematics.
2 * 3 + 4
2 + 3 * 4
2 / 3 + 4
2 + 3 / 4

// Braces are directly and strictly used by Scilab.
2 * (3 + 4)
(2 + 3) * 4
(2 + 3) / 4
3 / (2 + 4)


// Exponents
// We can also use the letter e
x = 1.23456789d10
// We can use positive exponents with the letter e
x = 1.23456789e10
// Or negative exponents
x = 1.23456789e-5

// Some elementary functions identities
sqrt(4)
sqrt(9)
sqrt(-1)
sqrt(-2)
exp(1)
log(exp(2))
exp(log(2))
10^2
log10(10^2)
10^log10(2)
sign(2)
sign(-2)
sign(0)

// Mathematical equalities does not always hold because of 
// floating point numbers. See section "Floating point numbers"
cos(0)
sin(0)
cos(%pi)
sin(%pi)
cos(%pi/4) - sin(%pi/4)

