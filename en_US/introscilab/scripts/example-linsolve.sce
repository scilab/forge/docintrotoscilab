// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// A cool example
A = [
    4    3    2    1  
    3    3    2    1  
    0    2    2    1  
    0    0    1    1  
];
b = [
10
9
5
2
];
x = linsolve ( A , -b )

// Create a Frank matrix
A = testmatrix ("frk" , 4)


// Compute relative error
xe = ones(4,1);
err = norm ( x - xe )/norm ( xe )

// Condition number
1/rcond(A)
%eps / rcond(A)

// A not cool example
n = 12
A = testmatrix ( "hilb", n );
xe = ones(n,1);
b = A * xe;
x = linsolve ( A , -b );
err = norm ( x - xe )/norm ( xe )
1/rcond(A)
%eps / rcond(A)

