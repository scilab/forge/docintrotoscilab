// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Examples of use of the surf function.

function z = f ( x , y ) 
  z = x./(1+y^2)
endfunction

n = 50
x = linspace ( -5,5,n);
y = linspace ( -5,5,n);
Z=eval3d(f,x,y);
surf(x,y,Z)


