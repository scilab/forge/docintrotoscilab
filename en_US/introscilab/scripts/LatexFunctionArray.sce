// Copyright (C) 2010 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Returns a Latex source code corresponding to the given list of functions.
// Parameters
//   funarray : a column matrix of strings, representing functions
//   ncols : the required number of columns in the Latex source code
//   formattingfun : the Latex macro corresponding with functions
function texsrc = LatexFunctionArray ( funarray , ncols , formattingfun )
  nfuns = size(funarray,"r")
  nrows = ceil(nfuns / ncols)
  // Compute the maximum number of characters of a column.
  // This allows to format a nicer table, with alighments.
  if ( formattingfun == [] ) then
    maxchar = max(length(funarray))
    fmt = "%-" + string(maxchar) + "s"
  else
    maxchar = max(length(funarray)) + length(formattingfun) + 3
    fmt = "%-" + string(maxchar) + "s"
  end
  // Create the LaTex source
  texsrc = []
  if ( nrows > 0 ) then
    texsrc ( 1 ) = ""
  end
  ifun = 0
  for irow = 1 : nrows
    texsrc ( irow ) = ""
    for icol = 1 : ncols
      ifun = ifun + 1
      if ( ifun <= nfuns ) then
        if ( formattingfun == [] ) then
          name = sprintf(fmt,funarray(ifun))
        else
          name = sprintf(fmt,"\" + formattingfun + "{" + funarray(ifun) + "}")
        end
      else
        name = sprintf(fmt,"")
      end
      texsrc ( irow ) = texsrc ( irow ) + name
      if ( icol == ncols ) then
        texsrc ( irow ) = texsrc ( irow ) + " \\"
      else
        texsrc ( irow ) = texsrc ( irow ) + " &"
      end
    end
  end
endfunction
  
funarray = [
"acos"
"acosd"
"acosh"
"acoshm"
"acosm"
"acot"
"acotd"
"acoth"
"acsc"
"acscd"
"acsch"
"asec"
"asecd"
"asech"
"asin"
"asind"
"asinh"
"asinhm"
"asinm"
"atan"
"atand"
"atanh"
"atanhm"
"atanm"
"cos"
"cosd"
"cosh"
"coshm"
"cosm"
"cotd"
"cotg"
"coth"
"cothm"
"csc"
"cscd"
"csch"
"sec"
"secd"
"sech"
"sin"
"sinc"
"sind"
"sinh"
"sinhm"
"sinm"
"tan"
"tand"
"tanh"
"tanhm"
"tanm"
];
ncols = 8;
formattingfun = "scifun";
texsrc = LatexFunctionArray ( funarray , ncols , formattingfun );
nrows = size(texsrc,"r");
for irow = 1 : nrows
  mprintf("%s\n",texsrc(irow))
end
//////////////////////////////////////////////////////////
funarray = [
"exp"
"expm"
"log"
"log10"
"log1p"
"log2"
"logm"
"max"
"maxi"
"min"
"mini"
"modulo"
"pmodulo"
"sign"
"signm"
"sqrt"
"sqrtm"
];
ncols = 8;
formattingfun = "scifun";
texsrc = LatexFunctionArray ( funarray , ncols , formattingfun );
nrows = size(texsrc,"r");
for irow = 1 : nrows
  mprintf("%s\n",texsrc(irow))
end

