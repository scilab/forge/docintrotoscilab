// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A demo of a Monte-Carlo process

function y = mynaivefunction ( x )
  y = x(1) + x(2)
endfunction

function ymean = mynaivemean ( nexp , f ) 
  experiments = rand(nexp,2);
  for k = 1 : nexp
    y(k) = f ( experiments(k,:) );
  end
  ymean = mean(y); 
endfunction

// Check our function
ymean = mynaivemean ( 1000 , mynaivefunction );
mprintf ("mean(y)=%f (expectation=%f)\n", ymean , 1);

// Measure performances
nexp_data = [1 10 10^2 10^3 10^4 10^5];
for i = 1:6
  nexp = nexp_data(i);
  timer();
  ymean = mynaivemean ( nexp , mynaivefunction );
  tt(i) = timer();
  mprintf ("i=%d, N=%d, t=%f\n", i , nexp , tt(i) );
end

// The vectorized function
function y = myfunction ( x )
  y = x(:,1) + x(:,2)
endfunction
function ymean = mymean ( nexp , f ) 
  experiments = rand(nexp,2);
  y = f ( experiments );
  ymean = mean(y); 
endfunction

// Check our function
ymean = mymean ( 1000 , myfunction );
mprintf ("mean(y)=%f (expectation=%f)\n", ymean , 1);

// Measure performances
nexp_data = [1 10 10^2 10^3 10^4 10^5];
for i = 1:6
  nexp = nexp_data(i);
  timer();
  ymean = mymean ( nexp , myfunction );
  tt(i) = timer();
  mprintf ("i=%d, N=%d, t=%f\n", i , nexp , tt(i) );
end


