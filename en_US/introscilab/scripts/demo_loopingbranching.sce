// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////////////////////////////////////////////////////////
// if statement
if ( %t ) then 
  disp("Hello !")
end

if ( %f ) then 
  disp("Hello !")
else
  disp("Goodbye !")
end

i = 2
if ( i == 2 ) then 
  disp("Hello !")
else
  disp("Goodbye !")
end

// A mistake
i = 2
if ( i = 2 ) then 
  disp("Hello !")
else
  disp("Goodbye !")
end

i = 2
if ( i == 1 ) then 
  disp("Hello !")
elseif ( i == 2 ) then 
  disp("Goodbye !")
else
  disp("Stay there !")
end

////////////////////////////////////////////////////////
// A select - case
i = 2
select i
case 1
  disp("One")
case 2
  disp("Two")
case 3
  disp("Three")
else
  disp("Other")
end

// A select - case with management of errors
i = -5;
select i
case 1
  disp("One")
case 2
  disp("Two")
case 3
  disp("Three")
else
  error ( "Unexpected value of the parameter i" )
end


////////////////////////////////////////////////////////
// for statement

// Make the sum of i = 1, 2, ..., 10

// With the sum function
sum(1:10)

// With a for
s = 0
for i = 1 : 10
  s = s + i
end

// With a backward for
s = 0
for i = 10 : - 1 : 1
  s = s + i
end

// With a for and a step = 2: s = 1 + 3 + 5 + 7 + 9 = 25
s = 0
for i = 1 : 2 : 10
  s = s + i
end

////////////////////////////////////////////////////////
// while statement

// With a while
s = 0
i = 1
while ( i<= 10 )
  s = s + i
  i = i + 1
end

// With a while and a break
s = 0
i = 1
while ( %t )
  if ( i > 10 ) then
    break
  end
  s = s + i
  i = i + 1
end

// With a while and a step = 2: s = 1 + 3 + 5 + 7 + 9 = 25
s = 0
i = 1
while ( i<= 10 )
  if ( modulo ( i , 2 ) == 0 ) then
    i = i + 1
    continue
  else
    s = s + i
    i = i + 1
  end
end
// The same algorithm with a vectorized function.
s = sum(1:2:10);

// A for statement
for i = 1 : 5
  disp(i)
end

for i = 1 : 2 : 5
  disp(i)
end

// A backward loop
for i = 5 : - 1 : 1
  disp(i)
end

////////////////////////////////////////////////////////
// for statement : loops on row matrices

// A loop over doubles
v = [1.5 exp(1) %pi];
for x = v
  disp(x)
end

// A loop over strings
for s = ["foo" "bar"]
  disp(s)
end

// A loop over polynomials
for s = [poly([0 1] , "x" , "roots") poly([1 2] , "x" , "roots") ]
  disp(s)
end

// A loop over the elements of a list
for s = list ( 1 , "foo", %pi )
  disp(s)
end

