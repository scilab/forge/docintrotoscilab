// Copyright (C) 2009 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


////////////////////////////////////////////////////////
// Create a matrix
A = [1 , 2 , 3 ; 4 , 5 , 6]
A = [1 2 3
4 5 6]
A=[]
A = ones(2,3)

////////////////////////////////////////////////////////
// When the index is an expression
// A(2,1)

A = testmatrix("hilb",2)
A( 2 , [1.0 1.1 1.5 1.9] ) 
//
i = 2
j = 1
A(i,j)
//
ones(1,1)
ones(1,(1-0.9)*10)
int((1-0.9)*10)
format(25)
1-0.9
(1-0.9)*10
//
A = testmatrix("hilb",4)
triu(A,-1)
triu(A,-1.5)
triu(A,int(-1.5))
//
triu(A,-2)
triu(A,floor(-1.5))

////////////////////////////////////////////////////////
// Query matrices
A = ones(2,3)
[nr,nc]=size(A)
A = ones(2,3)
size(A,"*")

////////////////////////////////////////////////////////
// Accessing the elements of a matrix
A = ones(2,3)
B =  2 * ones(2,3)
A+B
A = ones(2,3)
A(1,1)
A(12,1)

////////////////////////////////////////////////////////
// The colon : operator
v = 2:4
v = 3:2:10
v = 10:-2:3
v = 3:-2:10
//
A = testmatrix("hilb",5)
A(1:2,3:4)

////////////////////////////////////////////////////////
// Access to many elements at once
A = testmatrix("hilb",5)
vi=1:2
vj=3:4
A(vi,vj)
vi=vi+1
vj=vj+1
A(vi,vj)

// Switch two rows of a matrix
A = testmatrix("hilb",3)
A([1 2],:) = A([2 1],:)

////////////////////////////////////////////////////////
// Matrices are dynamic
A = [1 2 3; 4 5 6]
A(3,1) = 7
A(:,3) = []
B = matrix(A,1,6)
////////////////////////////////////////////////////////
// The $ operator
A=testmatrix("hilb",3)
A($-1,$-2)
A($+1,:) = [1 2 3]

////////////////////////////////////////////////////////
// The eye matrix
A = ones(3,3)
B = A + 3*eye()
//
A = ones(2,2)
B = eye(A)
C = eye(3,2)



