// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Examples of use of the contour function.

// An example without problem
function f = myquadratic2arg ( x1 , x2 )
  f = x1**2 + x2**2;
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
contour ( xdata , ydata , myquadratic2arg , 10)

// A naive example with two loops
function f = myquadratic1arg ( x )
  f = x(1)**2 + x(2)**2;
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
for i = 1:length(xdata)
    for j = 1:length(ydata)
      x = [xdata(i) ydata(j)].';
      zdata ( i , j ) = myquadratic1arg ( x );
    end
end
contour ( xdata , ydata , zdata , [0.1 0.3 0.5 0.7])

// An efficient example with feval
function f = myquadratic1arg ( x )
  f = x(1)**2 + x(2)**2;
endfunction
function f = myquadratic3 ( x1 , x2 )
  f = myquadratic1arg ( [x1 x2] )
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
zdata = feval ( xdata , ydata , myquadratic3 );
contour ( xdata , ydata , zdata , [0.1 0.3 0.5 0.7])

// Directly pass the intermediate function to the contour function
function f = myquadratic1arg ( x )
  f = x(1)**2 + x(2)**2;
endfunction
function f = myquadratic3 ( x1 , x2 )
  f = myquadratic1arg ( [x1 x2] )
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
contour ( xdata , ydata , myquadratic3 , [0.1 0.3 0.5 0.7])


// An "naive" example where the function takes x1 and x2 as separate arguments.
function f = myquadratic2arg ( x1 , x2 )
  f = x1**2 + x2**2;
endfunction
xmin = -1.0
xmax = 1.0
stepx = 0.1
ymin = -1.0
ymax = 1.0
stepy = 0.1
nx = 100
ny = 100
stepx = (xmax - xmin)/nx;
xdata = xmin:stepx:xmax;
stepy = (ymax - ymin)/ny;
ydata = ymin:stepy:ymax;
contour ( xdata , ydata , myquadratic2arg , 10)

// An efficient example based on linspace where the function takes x1 and x2 as separate arguments.
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
contour ( xdata , ydata , myquadratic , 10 )

// An example of what must NOT be done to produce the 
// xdata.
xmin = -5;
xmax = 10;
stepx = 0.1;
nx = 100;
xdata = zeros ( nx );
for i = 1 : nx
  xdata(i) = xmin + (i-1)*(xmax-xmin)/(nx-1);
end

// A na�ve example where the function takes one single vector input argument
function f = myquadratic1arg ( x )
  f = x(1)**2 + x(2)**2;
endfunction
xdata = linspace ( -1 , 1, 100 );
ydata = linspace ( -1 , 1, 100 );
for ix = 1:length(xdata)
    for iy = 1:length(ydata)
      x = [xdata(ix) ydata(iy)].';
      zdata ( ix , iy ) = myquadratic1arg ( x );
    end
end
contour ( xdata , ydata , zdata , [0.1 0.3 0.5 0.7])

// An example with an intermediate function
function f = myquadratic1arg ( x )
  f = x(1)**2 + x(2)**2;
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
for ix = 1:length(xdata)
    for iy = 1:length(ydata)
      x = [xdata(ix) ydata(iy)].';
      zdata ( ix , iy ) = myquadratic2 ( x );
    end
end
contour ( xdata , ydata , zdata , [0.1 0.3 0.5 0.7])

// An efficient example where the function takes one single vector input argument
function f = myquadratic3 ( x1 , x2 )
  f = myquadratic1arg ( [x1 x2] )
endfunction
xdata = linspace ( -1 , 1 , 100 );
ydata = linspace ( -1 , 1 , 100 );
zdata = feval ( xdata , ydata , myquadratic4 );
contour ( xdata , ydata , zdata , [0.1 0.3 0.5 0.7])



